# vagrant+ansible

### ここを読む前に
vagrantってなに？ansibleってなに？って場合は下のやつをみてくだしあ。

- ~~vagrant基礎編(編集中)~~
- [ansible基礎編](http://192.168.1.165:3000/59815fa843d22001f1a97dca)

---

### 前提
- vagrant+ansibleとしてますが、local・develop・productサーバーで使う前提なのでansibleメインです。
  ~~ローカルではvagrant upにてansibleも実行されるようにしていますが、それ以外の時は~~基本的にはplaybookで実行してください。

- ここではcentosでの実行を前提にしてます。
  UbuntuやらDebianとかのやつは随時コミットお待ちしております

- apache・php・mysqlのroleのtaskはansible-galaxyを使ってます。

---

### インストール要件
vagrant・ansibleのインストールはすんでいるものとします。(ansible-galaxyも)

```
brew install http://git.io/sshpass.rb
```

これ入れてないとansibleからsshを使ったログインのパスワード入力でこける  
※win・mac両方とも

`to use the 'ssh' connection type with passwords, you must install the sshpass program`  

こんなエラーでます

> 参考URL
  http://akiyoko.hatenablog.jp/entry/2013/12/16/020529'

---

###使い方
とりあえずこれをクローンする

```
git clone hogehoge
```

Vagrantfileを編集する

```
./Vagrantfile

VAGRANTFILE_API_VERSION = "2"
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "hoge"
  config.vm.network "private_network", ip: "192.168.hoge.hoge"
  # config.vm.provision "ansible" do |ansible|
  #   ansible.playbook = "provision/playbook.yml"
  #   ansible.inventory_path = "provision/hosts"
  #   ansible.limit = 'all'
  # end
end
```
hogeのところは適宜変更で

ansibleの諸々を変更する

```
./provision/hosts

[web_servers]
192.168.hoge.hoge

[web_servers:vars]
ansible_ssh_port=22
ansible_ssh_user=vagrant
ansible_ssh_pass=vagrant
ansible_sudo_pass=vagrant
backup=yes
```

```
./provision/group_vars/all

locale: ja_JP.UTF-8
zone: Asia/Tokyo
zoneinfo_path: /usr/share/zoneinfo/Asia/Tokyo
```

```
./provision/group_vars/web_servers

# 入れたいphpのリポジトリを定義
# 例 remi,remi-php71
php_enablerepo: hoge

# 関連パッケージの定義
# 定義しない場合galaxyのパッケージのデフォルト値が適用される
# 例 php_packages: [php-devel,php-gd,php-json,php-mbstring,php-mysql,php-pdo,php-process,php-xml]

# php入れたままのiniに設定を上書きしたい場合はここをfalse
# trueの場合galaxyのパッケージのデフォルト値が適用される
php_use_managed_ini: true

# php.iniの設定を定義 以下サンプル
# expose_php: "off"
# max_execution_time: "60"
# max_input_time: "120"
# max_input_vars: "10000"
# post_max_size: "16M"
# upload_max_filesize: "16M"
# date.timezone: "Asia/Tokyo"
```

hogeのところは適宜変更で
各ソフトウェアの諸々を変更したら

```
vagrant up # vagrantで構築する場合
ansible-playbook provision/playbook.yml -i provision/hosts
```

を実行します。

共通設定のところでselinuxを無効にしてサーバーの再起動しているのですが、
centOS7の場合shuddownコマンド投げた瞬間にansibleがunreachbleというエラーを拾ってとまってしまいます。
とりあえずもう一回

```
ansible-playbook provision/playbook.yml -i provision/hosts
```

を実行してください。
そして誰か解決方法をみつけてください。

```
./provision/roles/common/main.yml

- name: サーバー再起動               ←これした瞬間止まってしまう(centOS7の時に確認)
  ignore_errors: true
  shell: sleep 2 && shutdown -r now
  when: selinux_status.stdout != 'Disabled'
  changed_when: false

- name: サーバー起動待ち
  wait_for_connection:
    sleep: 30
    delay: 5
    timeout: 300
  when: selinux_status.stdout != 'Disabled'
```

お願いします。

とりあえずこれでソフトウェアのインストールと初期設定が終わっているはずなので、
一応サーバーに接続して各バージョン等確認してください。
virtualhostとかの設定も各自お願いします。

プロジェクトで使いまわしたい場合、
gitで管理するなら何も考えずに設定したものをリモートにおけば大丈夫です。
それ以外の場合は、.gitignoreに書いてあるディレクトリを除いて共有してください。

MySQLの諸々に関して
MySQLをansibleでどうこうするのに今回(8/7 現在)時間が足りなかったので、
とりあえずインストールするまでの処理しかしていないです。(もしかしたらバージョンによっては普通に動くかも。5.6以上は動かない可能性高いです)
なので、ansibleの実行が終わったらサーバーに接続してmysqlの初期設定をお願いします。
my.cnf自体はテンプレートにて上書きを行っているので、それ以外のmysql_secure_installationとかの実行は各自お願いします。

このあたりissueにおいておくので誰かよくしていってください。

以上です。

---

### apacheの諸々に関して

今回使っているgalaxyのパッケージは、apacheの設定を変数で指定できるようになっています。
特殊な設定をしない限りは、デフォルトのままでいいかと思います。

変える必要がありそうなところとしてですが、virtual hostの設定をテンプレートとして持っているので、
そのあたり個人で作成してもらえればと思います。

apachのバージョンに関しては、自動で決定されます。
もしかしたらどこかで明示的に指定できるかもです。

---

### 各ソフトウェアの設定に関して

今回使っているgalaxyのパッケージは、php.ini等の設定を変数で指定できるようになっています。

```
./provision/group_vars/web_servers

# If this is set to false, none of the following options will have any effect.
# Any and all changes to /etc/php.ini will be your responsibility.

php_expose_php: "On"
php_memory_limit: "256M"
php_max_execution_time: "60"
php_max_input_time: "60"
php_max_input_vars: "1000"
php_realpath_cache_size: "32K"

php_error_reporting: "E_ALL & ~E_DEPRECATED & ~E_STRICT"
php_display_errors: "Off"
php_display_startup_errors: "Off"
```

上記のように変数を上書きすることで適用されます。
変数にどのようなものがあるのか確認するにはそれぞれのディレクトリの`defalts/main.yml`を参照してください。

追記： 設定の上書きが聞いていないことが判明。issueにおいておくので誰か解決お願いします。

---

### 現状手作業が必要な部分

- mysqlの初期設定(mysql_secure_installation もしかしたら他にあるかも)

---

### 今後できればいいなと思うこと

- centOS以外でも使えるようにしたい
  galaxyのパッケージ的にはその辺りも考慮してあるので試しにやってみればいけるかも
  commonのtasksとかは間違いなくこけます！

- 設定周りを整理したい
  特にapacheのvhostのテンプレとか規約作って社内テンプレ作りたい
